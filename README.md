# Mongoose OS Gas Monitor

## TODO

- Boot without alarm (x minutes)
- on/off alarm from button and from mqtt
- change alarm thresh from mqtt

## Overview

Hardware:
- NodeMCU (esp32 board)
- YL-44 (buzzer)
- TM-1637 (display)
- MQ-2 (gas sensor)

## How to install this app

- Install and start [mos tool](https://mongoose-os.com/software.html)
- Switch to the Project page, find and import this app, build and flash it:

<p align="center">
  <img src="https://mongoose-os.com/images/app1.gif" width="75%">
</p>

## For Remote Monitoring

- Set WIFI
- Set MQTT


## Case
Not for this project, but fitted almost as a glove

https://www.thingiverse.com/thing:4543826
