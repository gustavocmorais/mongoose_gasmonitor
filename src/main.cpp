

#include <stdio.h>
#include <math.h>
#include "mgos.h"
#include "mgos_mqtt.h"
#include "mgos_adc.h"
#include "common/json_utils.h"
#include <TM1637Display.h>
#include "Adafruit_SSD1306.h"


enum {
  ERROR_UNKNOWN_COMMAND = -1,
  ERROR_I2C_NOT_CONFIGURED = -2,
  ERROR_I2C_READ_LIMIT_EXCEEDED = -3
};

int test_buzzer = 0;
int last_sensor = 0;

TM1637Display *display;// ( mgos_sys_config_get_display_tm1637_clk(), mgos_sys_config_get_display_tm1637_pin(), 100);
Adafruit_SSD1306 *mySSD1306;

void update_display_tm1637 (int value) {

  LOG(LL_INFO, ("Will update tm1637"));

  display->setBrightness(0x0f);
  display->showNumberDec(value, false); 
  
}

void update_display_ssd1306 (int value) {

  LOG(LL_INFO, ("Will update ssd1306"));

  mySSD1306->clearDisplay();
  mySSD1306->setTextSize(3);
  mySSD1306->setTextColor(WHITE);
  mySSD1306->setCursor(mySSD1306->width() / 4, mySSD1306->height() / 4);
  mySSD1306->printf("%d", value);
  mySSD1306->display();
  
}

static void set_buzzer(int new_status) {
  mgos_gpio_set_mode(mgos_sys_config_get_gasmonitor_relay_pin(), MGOS_GPIO_MODE_OUTPUT);
  mgos_gpio_write(mgos_sys_config_get_gasmonitor_relay_pin(), !new_status);
}


static double read_sensor() {
  double x = mgos_adc_read(0);

  LOG(LL_INFO, ("sensorValue: %lf", x));

  return x;
}


 
static void net_cb(int ev, void *evd, void *arg) {
  switch (ev) {
    case MGOS_NET_EV_DISCONNECTED:
      LOG(LL_INFO, ("%s", "Net disconnected"));
      break;
    case MGOS_NET_EV_CONNECTING:
      LOG(LL_INFO, ("%s", "Net connecting..."));
      break;
    case MGOS_NET_EV_CONNECTED:
      LOG(LL_INFO, ("%s", "Net connected"));
      break;
    case MGOS_NET_EV_IP_ACQUIRED:
      LOG(LL_INFO, ("%s", "Net got IP address"));
      break;
  }
 
  (void) evd;
  (void) arg;
}

static void report_to_mqtt() {
  char topic[100], message[512];
  struct json_out out = JSON_OUT_BUF(message, sizeof(message));
   
  time_t now=time(0);
  struct tm *timeinfo = localtime(&now);
 
  snprintf(topic, sizeof(topic), "things/%s/status",mgos_sys_config_get_device_id());
  json_printf(&out, "{total_ram: %lu, free_ram: %lu, sensor: %lf, device: \"%s\", timestamp: \"%02d:%02d:%02d\"}",
              (unsigned long) mgos_get_heap_size(),
              (unsigned long) mgos_get_free_heap_size(),
              (float) read_sensor(),
              (char *) mgos_sys_config_get_device_id(),
              (int) timeinfo->tm_hour,
              (int) timeinfo->tm_min,
              (int) timeinfo->tm_sec);
  bool res = mgos_mqtt_pub(topic, message, strlen(message), 1, false);
  LOG(LL_INFO, ("Published to MQTT 1: %s", res ? "yes" : "no"));

  

}

static void report_status(void *arg) {
  report_to_mqtt();
  (void) arg;
}


static void check_status (void *arg) {
  
  float x = read_sensor();

  if (mgos_sys_config_get_display_type() == 1) {
    update_display_tm1637((int)x);
  } 
  else {
    update_display_ssd1306((int)x);
  }

  if (x >= mgos_sys_config_get_gasmonitor_limit()) {
    LOG(LL_INFO, ("achei gas!"));
    report_status(NULL);

    if (mgos_sys_config_get_gasmonitor_alarm_enabled()) {
      test_buzzer = !test_buzzer;
      set_buzzer(test_buzzer);
    }

  }
  else if (abs(x - last_sensor) > 50 ) {
    LOG(LL_INFO, ("reporting small change!"));
    report_status(NULL);
    set_buzzer(0); //buzzer off

  }
  
  else {
    LOG(LL_INFO, ("sem acao..."));
    set_buzzer(0); //buzzer off
  }

  last_sensor = x;

  

  //report_status();
  (void) arg;
}


 
static void sub(struct mg_connection *c, const char *fmt, ...) {
  char buf[100];
  struct mg_mqtt_topic_expression te = {.topic = buf, .qos = 1};
  uint16_t sub_id = mgos_mqtt_get_packet_id();
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buf, sizeof(buf), fmt, ap);
  va_end(ap);
  mg_mqtt_subscribe(c, &te, 1, sub_id);
  LOG(LL_INFO, ("Subscribing to %s (id %u)", buf, sub_id));
}


static void ev_handler(struct mg_connection *c, int ev, void *p,
                       void *user_data) {
  struct mg_mqtt_message *msg = (struct mg_mqtt_message *) p;

  if (ev == MG_EV_MQTT_CONNACK) {
    LOG(LL_INFO, ("CONNACK: %d", msg->connack_ret_code));
    sub(c, "things/%s/action",mgos_sys_config_get_device_id());
    report_to_mqtt();

  } 
  
  else if (ev == MG_EV_MQTT_SUBACK) {
    LOG(LL_INFO, ("Subscription %u acknowledged", msg->message_id));
  } 
  
  else if (ev == MG_EV_MQTT_PUBLISH) {
    struct mg_str *s = &msg->payload;

    LOG(LL_INFO, ("got command: [%.*s]", (int) s->len, s->p));
    /* Our subscription is at QoS 1, we must acknowledge messages sent ot us. */
    mg_mqtt_puback(c, msg->message_id);

    char *action = 0;
    
    //veio ACTION
    if (json_scanf(s->p, s->len, "{action: %Q}", &action) > 0 ) {
      LOG(LL_INFO, ("action: %s", action)); 

      //reporta status
      if (!strcmp(action, "REPORT")) {
        report_to_mqtt();
      }

    }
    
    else {
      LOG(LL_INFO, ("error command: unknown"));
      //pub(c, "{error: {code: %d, message: %Q}}", ERROR_UNKNOWN_COMMAND,
      //    "unknown command");
    }

  }
  (void) user_data;
}



enum mgos_app_init_result mgos_app_init(void) {

  
  if (mgos_sys_config_get_display_type() == 1) {
    LOG(LL_INFO, ("Will USE tm1637"));
    display = new TM1637Display ( mgos_sys_config_get_display_tm1637_clk(), mgos_sys_config_get_display_tm1637_pin(), 100);
    update_display_tm1637(0);
  } 
  else {
    LOG(LL_INFO, ("Will USE ssd1306"));
    mySSD1306 = new Adafruit_SSD1306(mgos_sys_config_get_display_ssd1306_rst() /* RST GPIO */, Adafruit_SSD1306::RES_128_32);
    if (mySSD1306 != nullptr) {
      LOG(LL_INFO, ("inicializando com sucesso"));
      mySSD1306->begin(SSD1306_SWITCHCAPVCC, 0x3C, false /* reset */);
      mySSD1306->display();
    }
  }

  /* Check change every second */
  mgos_set_timer(1000, MGOS_TIMER_REPEAT, check_status, NULL);

  /*report every minute*/
  mgos_set_timer(60000, MGOS_TIMER_REPEAT, report_status, NULL);

  
 
  /* Network connectivity events */
  mgos_event_add_group_handler(MGOS_EVENT_GRP_NET, net_cb, NULL);

  mgos_mqtt_add_global_handler(ev_handler, NULL);

  mgos_adc_enable(0);

  //reboot every 12h
  mgos_system_restart_after(43200*1000);

  

  


  check_status(NULL);

  return MGOS_APP_INIT_SUCCESS;
}


